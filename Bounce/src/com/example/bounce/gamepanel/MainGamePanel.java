package com.example.bounce.gamepanel;



import com.example.bounce.MainThread;
import com.example.bounce.model.Ball;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

public class MainGamePanel extends SurfaceView 
implements SurfaceHolder.Callback{

	static final int BALL_CNT = 200;
	private static final String TAG = MainGamePanel.class.getSimpleName();
	private MainThread thread;
	private Ball[] ball;
	private String avgFps;
	
	public static int ScreenWidth = 0;
	public static int ScreenHeight = 0;
	
	public static int getWidth(Context mContext){
	    int width=0;
	    WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
	    Display display = wm.getDefaultDisplay();
	    if(Build.VERSION.SDK_INT>12){                   
	        //Point size = new Point();
	        //display.getSize(size);
	        //width = size.x;
	    }
	    else{
	        width = display.getWidth();  // deprecated
	    }
	    return width;
	}
	
	public static int getHeight(Context mContext){
	    int height=0;
	    WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
	    Display display = wm.getDefaultDisplay();
	    if(Build.VERSION.SDK_INT>12){               
	        //Point size = new Point();
	        //display.getSize(size);
	        //height = size.y;
	    }else{          
	        height = display.getHeight();  // deprecated
	    }
	    return height;      
	}
	
	public MainGamePanel(Context context) {
		super(context);
		Log.d(TAG,"Create Main Game Panel!");
		getHolder().addCallback(this);
		thread = new MainThread(getHolder(), this);
		ScreenWidth = getWidth(context);// ������ �� ������ ������
		ScreenHeight = getHeight(context);
		ball = new Ball[BALL_CNT];
		for(int i=0;i<BALL_CNT; i++)
		{
			ball[i] = new Ball(ScreenWidth, ScreenHeight);
		}
		thread.run();
		setFocusable(true);
	}
	

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		thread.setRunning(true);
		thread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		Log.d(TAG,"Surface is being destroyed");
	}

	public void Update(){
		for(int i=0;i<BALL_CNT; i++)
		{
			ball[i].update();
		}
	}
	
	public void Render(Canvas canvas){
		canvas.drawColor(Color.BLACK);
		for(int i=0;i<BALL_CNT; i++)
		{
			ball[i].render(canvas);
		}
		displayFps(canvas, avgFps);
	}

	public void setAvgFps(String avgFps) {
		this.avgFps = avgFps;
	}
	
	
	private void displayFps(Canvas canvas, String fps){
		//
		if(canvas != null && fps != null){
			//Log.d(TAG, "Display FPS");
			Paint paint = new Paint();
			paint.setARGB(255, 255, 255, 255);
			//canvas.drawColor(Color.WHITE);
			canvas.drawText(fps, this.getWidth() - 50, 20, paint);
		}
		else if(canvas==null){Log.d(TAG, "canvas==null");}
		else if(fps==null){Log.d(TAG, "fps==null");}
	}
	
}
