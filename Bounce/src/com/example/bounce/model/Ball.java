package com.example.bounce.model;

import java.util.Random;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;

public class Ball {
	private static final String TAG = Ball.class.getSimpleName();
	
	public double x,y;
	public double vx,vy;
	public double dt = 0.3;
	public double v;
	
	public static double MAX_V = 4;
	public static Random random = new Random();
	public int width,height;

	static double vmax = 0;
	public int R,G,B;
	
	static private double g = 0.25; //gravity
	static private float radius = (float) 7.0;//ball radius
	static private double alfa = 0.999;
	
	// empty method
	public newMethod(){
		return 0;
	}
	
	public Ball(int width,int height){
		this.width = width;
		this.height = height;
		x = width*random.nextDouble();
		y = height*random.nextDouble();
		vx = (random.nextDouble()-0.5)*2*MAX_V;
		vy = (random.nextDouble()-0.5)*2*MAX_V;
		R =255;// (int)(255.0*random.nextDouble());
		G =0; //(int)(255.0*random.nextDouble());
		B =255;// (int)(255.0*random.nextDouble());
		
		vmax = Math.sqrt(2*g*height+MAX_V*MAX_V);
	}
	
	public void update(){
		x+=vx*dt;
		y+=vy*dt;
		
		vy+=g*dt;
		
		v = Math.sqrt(vx*vx+vy*vy);
		
		//Log.d(TAG,"x = "+x+"   y = "+y);
		if(x>width)vx = -Math.abs(vx)*alfa;
		if(x<0)vx = Math.abs(vx)*alfa;
		if(y<0)vy = Math.abs(vy*alfa);
		if(y>height)vy = -Math.abs(vy)*alfa;
	}

	public void render(Canvas canvas) {
		Paint paint = new Paint();
		int r,g,b;
		/*r = (int)((double)R*(v+vmax/3)/(4/3*vmax));
		g = (int)((double)G*(v+vmax/3)/(4/3*vmax));
		b = (int)((double)B*(v+vmax/3)/(4/3*vmax));*/
		r = (int)(255.0*v/vmax);
		g = 0;
		b = (int)(255*(1-v/vmax));
		paint.setARGB(255, r, g, b);
		
		canvas.drawCircle((float)x, (float)y, radius, paint);
	}
}
